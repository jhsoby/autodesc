var fs = require('fs');
var http = require('http');
var url = require("url");
var Qs = require('qs');
var autodesc = require('./auto_long_desc.js') ;

var port = parseInt(process.env.TOOL_WEB_PORT, 10) ;

var html_header = '<!DOCTYPE html><html><head><meta charset="utf-8"> </head><body>' ;


http.createServer(function (req, res) {
	var pathname = url.parse(req.url).pathname.replace(/\/\w+/,'');
	var params = Qs.parse(url.parse(req.url).query) ;
	
	if ( typeof params.q != 'undefined' && params.q != '' ) {
		params.q = 'Q'+(''+params.q).replace(/\D/g,'') ;
		var use_lang = params.lang=='any'?undefined:params.lang ;
		if ( params.lang == '' || params.lang == 'any' ) params.lang = autodesc.defaults.language ;
		autodesc.getDescription ( params , function ( text ) {
			var j = {
				call : params ,
				q : params.q ,
				label : autodesc.wd.items[params.q].getLabel(use_lang) ,
				manual_description : autodesc.wd.items[params.q].getDesc(use_lang) ,
				result : text
			} ;
			if ( params.debug ) j.rand = Math.random() ;
			
			function fin () {
				if ( params.format == 'html' ) {
					res.writeHead(200, {'Content-Type': 'text/html; charset=utf-8','Cache-Control':'no-cache, must-revalidate'});
					res.write ( html_header ) ;
					res.write ( "<style>a.redlink { color:red }</style>" ) ;
					res.write ( "<h1>" + j.label + " (<a href='//www.wikidata.org/wiki/" + j.q + "'>" + j.q + "</a>)</h1>" ) ;
					if ( params.links == 'wiki' ) res.write ( "<pre style='white-space:pre-wrap;font-size:11pt'>" + j.result + "</pre>" ) ;
					else res.write ( "<p>" + j.result + "</p>" ) ;
					res.write ( "<hr/><div style='font-size:8pt;'>This text was generated automatically from Wikidata using <a href='/autodesc/?'>AutoDesc</a>.</div>" ) ;
					res.write ( "</body></html>" ) ;
				} else if ( params.format == 'jsonfm' ) {
					var json_link = [] ;
					$.each ( params , function ( k , v ) {
						json_link.push ( k + "=" + escape ( k=='format'?'json':v ) ) ;
					} ) ;
					json_link = "<a href='?" + json_link.join('&') + "'>format=json</a>" ;
					var s = html_header ;
					s += "<p>You are looking at the HTML representation of the JSON format. HTML is good for debugging, but is unsuitable for application use.</p>" ;
					s += "<p>Specify the format parameter to change the output format. To see the non-HTML representation of the JSON format, set " + json_link + ".</p>" ;
					s += '<hr/><pre style="white-space:pre-wrap">' ;
					s += JSON.stringify(j,null,4) ;
					s += '</pre>' ;
					s += '</body></html>' ;
					res.writeHead(200, {'Content-Type': 'text/html; charset=utf-8','Cache-Control':'no-cache, must-revalidate'});
					res.write ( s ) ;
				} else {
					res.writeHead(200, {'Content-Type': 'application/json; charset=utf-8','Cache-Control':'no-cache, must-revalidate'});
					if ( typeof params.callback != 'undefined' ) res.write ( params.callback + "(" ) ;
					res.write ( JSON.stringify(j) ) ;
					if ( typeof params.callback != 'undefined' ) res.write ( ")" ) ;
				}
				res.end() ;
			}
			
			if ( typeof params.media != 'undefined' && params.media ) autodesc.addMedia ( j , params.thumb , fin , params.zoom ) ;
			else fin() ;
			
		} ) ;
		return ;
	}

	fs.readFile('index.html', 'utf8', function (err,data) {
		res.writeHead(200, {'Content-Type': 'text/html; charset=utf-8'});
	//	res.write(pathname+"\n\n") ;
	//	res.write ( JSON.stringify(params) ) ;
	//	res.end('\n 5 Hello World\n');

		res.end(data) ;
	} ) ;
	
	

}).listen(port);

console.log('Server running at port '+port);

